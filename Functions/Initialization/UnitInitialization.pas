{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}
{
WineLanchers folder layout:
$home/.local/
             wine/
                  git/
                  wine version/
             wine-prefix/
                         prefix name/
                                     PrefixData.xml
$home/.config/
              WineLancher/
                          LastUsedConfig.xml

Layout changes that need doing:
Replace '$home/.config/WineLancher/1' with some system that does not need a file.
Remove '$home/.config/WineLancher/SetupGit'.
Merge 'StringGrid_TerminalSettings' with 'LastUsedConfig.xml'.
}

unit UnitInitialization;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLVersion, Process, AsyncProcess, ComCtrls, Forms;


procedure InitializationCode;

const
 WineLauncherVersion = {$I VERSION.inc};
 BuiltWithFpcVersion = ('Built with FPC version: ' + {$I %FPCVERSION%});
 BuiltWithLazarusVersion = ('Built with lazarus version: ' + lcl_version);
 WineLauncherWinePrefixes = ('WINELAUNCHER_WINE_PREFIXES');

var
 {Home folder}
  Home:string;
 {PathToPrefix}
  PathToPrefix:string;
 {"$HOME/.config/WineLauncher"}
  ConfigPath:string;
 {Used in "RunWineCheck".}
  AProcess:TProcess;
 {Cut down on the spam.}
  HideChannel:boolean;
 {Used for scaning cd drive.}
  AsyncProcessScan:TAsyncProcess;
 {Path to Wine folders}
  PathToWine:string;
 {CreatePrefixProcess}
  CreatePrefixProcess:TProcess;
 {This array keeps track of tabs uses in form1.}
  TabSheets:array of TTabSheet;
 {If the is no preexisting prefix then this boolean is set to true.}
  NoPrefix:boolean;

implementation
uses
 UnitMisc;

procedure InitializationCode;
begin
     {Cut down on the logging spam.}
     HideChannel := true;

     {Home needs to exist.}
     Home := GetEnvironmentVariable('HOME');
     if DoesFolderExists(Home) = false then Application.Terminate;

     {Make sure all the folders we need exist and try to create then if they do not exist.}
     ConfigPath := (GetAppConfigDir(false));
     if ForceDirectories(ConfigPath) = false then Application.Terminate;

     if DirExistsIfNotMakeIt(Home + '/.local/') = false then Application.Terminate;

     PathToWine := (Home + '/.local/wine/');
     if DirExistsIfNotMakeIt(PathToWine) = false then Application.Terminate;

     {Check if WineLauncherWinePrefixes should be used.}
     if GetEnvironmentVariable(WineLauncherWinePrefixes) <> '' then
     begin
          PathToPrefix := IncludeTrailingPathDelimiter(GetEnvironmentVariable(WineLauncherWinePrefixes));
     end
        else
            begin
                 PathToPrefix := (Home + '/.local/wine-prefix/');
            end;

     if DirExistsIfNotMakeIt(PathToPrefix) = false then Application.Terminate;

     {Used in RunWineCheck.}
     AProcess := TProcess.Create(nil);

     CreatePrefixProcess := TProcess.Create(nil);
     CreatePrefixProcess.Options := CreatePrefixProcess.Options + [poWaitOnExit, poUsePipes];

     {Used for scaning cd drive.}
     AsyncProcessScan := TAsyncProcess.Create(nil) ;
     AsyncProcessScan.Options := AsyncProcessScan.Options + [poUsePipes,poStderrToOutPut,poNoconsole,poNewProcessGroup];
end;

end.

