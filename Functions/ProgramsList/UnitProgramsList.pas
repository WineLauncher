{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

unit UnitProgramsList;

{$mode objfpc}{$H+}


interface

uses
  Classes, SysUtils, XMLCfg, Grids;

  function ProgramsListSavePrefix(PrefixName:string):boolean;
  function ProgramsListLoadPrefix(PrefixName:string):boolean;
  function SetUpGrid(A:integer):boolean;
  function SetUpArray():boolean;

type
    DataStore = record
     PrefixName:string;
     Grid:TStringGrid;
    end;

var
   PreFixList:array of Tstrings;
   CdData:Tstrings;
   CdDataName:Tstrings;
   Data:array of DataStore;

const
     DataNameCol = 0;
     DataPathCol = 1;
     DataFlagsCol = 2;
     PrefixDataName = 'PrefixData.xml';

implementation
uses
  UnitSettings,
  UnitMisc,
  UnitMain;

function ProgramsListSavePrefix(PrefixName:string):boolean;
var
   PathWithName:string;
   FullPathWithName:string;
   loop:integer;
   PrefixInt:integer;
   SaveConfig:TXMLConfig;
begin
     PrefixInt := FindPrefixInt(PrefixName);

     {Stop if the prefix does not exist.}
     if PrefixInt = -1 then exit;

     {Get the prefix path.}
     PathWithName := (PreFixList[1].Strings[PrefixInt]);
     {Stop if the prefix path does not exist.}
     if DoesFolderExists(PathWithName) = false then exit(false);

     FullPathWithName := (PathWithName + '/' + PrefixDataName);

     {Delete the old file.}
     if FileExists(FullPathWithName) then DeleteFile(FullPathWithName);
     SaveConfig := TXMLConfig.Create(nil);
     Try
        SaveConfig.FileName := FullPathWithName;

        SaveConfig.SetValue('Version', 6);
        SaveConfig.SetValue('PrefixName', PrefixName);

        {The frist row is the header}
        for loop := 1 to (data[PrefixInt].Grid.Cols[DataNameCol].Count -1) do
            begin
                 {Do not save blank items.}
                 if data[PrefixInt].Grid.Cells[DataNameCol,loop]  = '' then exit;

                 {The items need to start at 0.}
                 SaveConfig.SetValue('Item' + InttoStr(loop - 1) + '/Name', data[PrefixInt].Grid.Cells[DataNameCol, loop]);
                 SaveConfig.SetValue('Item' + InttoStr(loop - 1) + '/Path', data[PrefixInt].Grid.Cells[DataPathCol, loop]);

                 if data[PrefixInt].Grid.Cells[DataFlagsCol, loop] <> '' then
                    begin
                         SaveConfig.SetValue('Item' + InttoStr(loop - 1) + '/Flags', data[PrefixInt].Grid.Cells[DataFlagsCol,loop] );
                    end;

            end;

     Finally
      SaveConfig.Flush;
      SaveConfig.Free;
     end;

end;

function ProgramsListLoadPrefix(PrefixName:string):boolean;
var
   PathWithName:string;
   FullPathWithName:string;
   LoopCount:integer;
   Done:boolean;
   PrefixInt:integer;
   LoadConfig:TXMLConfig;
begin
     PrefixInt := FindPrefixInt(PrefixName);

     {Stop if the prefix does not exist.}
     if PrefixInt = -1 then exit;

     {Get the prefix path.}
     PathWithName := (PreFixList[1].Strings[PrefixInt]);

     FullPathWithName := (PathWithName + '/' + PrefixDataName);

     LoadConfig := TXMLConfig.Create(nil);

     if FileExists(FullPathWithName) = true then
        begin
              Try
                 LoadConfig.FileName := FullPathWithName;

                 {The items start at 0.}
                 LoopCount := 0;
                 Done := false;

                 if LoadConfig.getValue('Version', -1) = 6 then
                    begin
                         while Done = false do
                             begin
                                  {Do not load blank items.}
                                  if LoadConfig.GetValue('Item' + InttoStr(LoopCount) + '/Name', '')  <> '' then
                                     begin
                                          {Leave room for the header.}
                                          data[PrefixInt].Grid.Cells[DataNameCol, LoopCount +1] := LoadConfig.GetValue('Item' + InttoStr(LoopCount) + '/Name', '');
                                          data[PrefixInt].Grid.Cells[DataPathCol, LoopCount +1] := LoadConfig.GetValue('Item' + InttoStr(LoopCount) + '/Path', '');

                                          if LoadConfig.GetValue('Item' + InttoStr(LoopCount) + '/Flags', '') <> '' then
                                             begin
                                                  data[PrefixInt].Grid.Cells[DataFlagsCol, LoopCount +1] := LoadConfig.GetValue('Item' + InttoStr(LoopCount) + '/Flags', '');
                                             end;
                                          inc(LoopCount);
                                     end
                                        else
                                            begin
                                                 Done := true;
                                            end;
                             end;
                    end;

              Finally
                     LoadConfig.Flush;
                     LoadConfig.Free;
              end;
        end;
end;

function SetUpGrid(A:integer):boolean;
begin
         {This can not be nil or the debugger will get upset when you close the program.}
         Data[A].Grid := TStringGrid.Create(UnitSettings.Form4);
         Data[A].Grid.RowCount := 101;
         Data[A].Grid.ColCount := 3;
         Data[A].PrefixName := PreFixList[0][A];

         {Fill in the header for the grid.}
         {Cells[Col, Row]}
         Data[A].Grid.Cells[DataNameCol,0] := 'Name';
         Data[A].Grid.Cells[DataPathCol,0] := 'Path';
         Data[A].Grid.Cells[DataFlagsCol,0] := 'Flags';
end;

function SetUpArray():boolean;
var
  loop:integer;
begin
     {Lay out for Data: array of DataStore}
     {Type DataStore: string, TStringGrid}

     SetLength(Data, PreFixList[0].count);
     for loop := 0 to (PreFixList[0].count -1) do
         begin
              SetUpGrid(loop);
         end;

     {Load from file(s) into the array}
     for loop := 0 to (PreFixList[0].Count -1) do
     begin
          ProgramsListLoadPrefix(PreFixList[0][loop]);
     end;

     {Force the Grid to update, if we do not you can get data lost.}
     UnitSettings.form4.ComboBox_PreFixChange(UnitSettings.form4);
     {Force the Prefix Combobox to update, or the ProgramsList comobox will not list anything.}
     UnitMain.form1.ComboBox_PreFixChange(UnitMain.form1);
end;

end.
