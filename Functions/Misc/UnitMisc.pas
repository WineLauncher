{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

unit UnitMisc;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, strutils, BaseUnix, Process, StdCtrls, FileUtil, Graphics;

  procedure Log(Level:integer; Channel:string; LogText:string);
  procedure ListFileDir({0} Path:string; {1} FileList:TStrings; {2} GroupList:TStrings; {3} DirListClean:TStrings; {4} DirListPath:TStrings; {5} isFileList:Boolean);
  procedure FullClear(ComboBox:TComboBox);
  {$IFDEF LogVar}
   procedure LogVar(MyVar:string; MyVarName:string);
  {$ENDIF}

  function Wrap(Input:string):string;  { Makes Paths look better, E.G. " ( Input ) " }
  function WinToUnixPath(Path:string):string;
  function UnixToWinPath(Path:string; DriveChar:Char):string;
  function GetUnixDirPath(FilePath:string):string;
  function FileExistsAndIsExecutable(FullPath:string; JustCheckExists:boolean): boolean ;
  function WorkDirTemplate(FolderPath:string):string;
  function DoesFolderExists(Path:string; FolderName:string): boolean;
  function DoesFolderExists(FullFolderPath:string): boolean;
  function DirExistsIfNotMakeIt(Path:string; FolderName:string ): boolean;
  function DirExistsIfNotMakeIt(PathWithFolder:string): boolean;
  function MakeFile(FullPath:string;Data:string):boolean;
  function SetExecutableFlag(FullPath:string):boolean;
  function ColourCheck(Input: TEdit):boolean;
  function CutUpFlags(Flags:string):boolean;
  function FindPrefixInt(PrefixName:string):integer;
  function GetWinFileName(FullPath:string):string;
  function FolderScan(ComboBox:TComboBox; Path:string):boolean;
  function SelectItem(ComboBox:TComboBox; ItemName:string):boolean;
  function SearchForBin(FileName:string):string;
  function FindWine(Path:string):string;
  function FCreatePrefixProcess(PrefixName:string):boolean;

implementation
uses
  UnitInitialization,
  UnitMain,
  UnitSettings,
  UnitProgramsList;

function FileExistsAndIsExecutable(FullPath:string; JustCheckExists:boolean): boolean ;
var
ChannelLocal:string;
begin
ChannelLocal := ('FileExistsAndIsExecutable');

if JustCheckExists = true then
   begin
        if FileExists(FullPath) then
        begin
             {$IFDEF MoreTrue }
              if JustCheckExists = true then Log(3, ChannelLocal, 'File' + Wrap(FullPath) + 'exists.');
             {$ENDIF}
             Result := true;
        end
           else
               begin
                    Log(4, ChannelLocal,'File' + Wrap(FullPath) + 'does not exists.');
                    Result := false;
               end;
   end
      else
          begin
               if FpAccess(FullPath, X_OK {Exe flag check}) <> 0 then
                 begin
                      Log(4, ChannelLocal,'File' + Wrap(FullPath) + 'is not executable.');
                      Result := false;
                 end
                    else
                        begin
                             {$IFDEF MoreTrue }
                              Log(3, ChannelLocal,'File' + Wrap(FullPath) + 'is executable.');
                             {$ENDIF}
                             Result := true;
                        end;

    end;
end;

function Wrap(Input: string):string;
begin
     Result := ( ' ( ' + Input + ' ) ' );
end;

function DoesFolderExists(Path:string; FolderName:string): boolean;
var
ChannelLocal:string;
begin
ChannelLocal := 'DoesFolderExists';
     if DirectoryExists(Path + FolderName) = true then
        begin
             Result := true ;
             {$IFDEF MoreTrue}
              Log(3, ChannelLocal, ('Folder' + Wrap(Path + FolderName) + 'exists.'));
             {$ENDIF}
        end
           else
               begin
                    Result := false ;
                    Log(4, ChannelLocal, ('Folder' + Wrap(Path + FolderName) + 'does not exists.'));
               end;
end;

function DoesFolderExists(FullFolderPath:string): boolean;
var
ChannelLocal:string;
begin
ChannelLocal := 'DoesFolderExists';
     if DirectoryExists(FullFolderPath) = true then
        begin
             Result := true ;
             {$IFDEF MoreTrue}
              Log(3, ChannelLocal, ('Folder' + Wrap(FullFolderPath) + 'exists.'));
             {$ENDIF}
        end
           else
               begin
                    Result := false ;
                    Log(4, ChannelLocal, ('Folder' + Wrap(FullFolderPath) + 'does not exists.'));
               end;
end;

procedure Log(Level:integer; Channel:string; LogText:string);
var
   LevelString:string;
   OutPut:string;
begin
     {Level 0 "Info"}
     {Level 1 "Error"}
     {Level 2 "Variable"}
     {Level 3 "Dev-Info"}
     {Level 4 "Dev-Error"}
     {Level 5 is for uses with custom functions.}

     Case Level of
       0: LevelString := 'Info';
       1: LevelString := 'Error';
       2: LevelString := 'Variable';
       3: LevelString := 'Dev-Info';
       4: LevelString := 'Dev-Error';
     end;

     if Level = 5 then
        begin
             OutPut := (LogText);
        end
           else
               begin
                   if HideChannel = true then
                      begin
                           OutPut := (LevelString + ': ' + LogText);
                      end
                         else
                             begin
                                  OutPut := (LevelString + ':' + Channel + ': ' + LogText);
                             end;
               end;
     {Output}
     WriteLn(OutPut);
     if UnitMain.form1 <> nil then
        begin
             UnitMain.form1.Memo_LogOutPut.Lines.Add(OutPut);
        end;
end;

function WinToUnixPath(Path:string):string;
var
PathUpToDosdevices:string;
DriveNameWithSlash:string;
FullUnixPath:string;
begin
     {Get the path up to Dosdevices.}
     PathUpToDosdevices := (PathToPrefix + UnitMain.form1.ComboBox_PreFix.Text + '/dosdevices/' );
     {Take the DriveName off 'path' and replace '\' with '/'.}
     DriveNameWithSlash := (AnsiLowerCase( Copy2SymbDel( Path, '\' )) + '/');
     {Replace all '\' to '/'.}
     Path := AnsiReplaceText(Path, '\', '/');
     {Sort then.}
     FullUnixPath := (PathUpToDosdevices + DriveNameWithSlash + Path);
{$IFDEF LogVar}
 {$IFDEF MOREDEBUG_WinToUnixPath}
  logVar(PathUpToDosdevices, 'PathUpToDosdevices');
  logVar(DriveNameWithSlash, 'DriveNameWithSlash');
  logVar(Path, 'Path');
 {$ENDIF}
 logVar(FullUnixPath, 'FullUnixPath');
{$ENDIF}
 Result := FullUnixPath;

end;


function UnixToWinPath(Path:string; DriveChar:Char):string;
var
WindowsFilePath :string;
SmallPath:string;
Cleanup:string;
begin
     if Path = '' then
        begin
             Result := '';
             exit;
        end;

     Path := AnsiReverseString(Path);
     SmallPath := Copy2SymbDel(Path, ':');
     SmallPath := AnsiReverseString(SmallPath);
     Cleanup := AnsiReplaceText(SmallPath, '/', '\');
     WindowsFilePath := (DriveChar + ':' + Cleanup);

     {$IFDEF LogVar}
      {$IFDEF MOREDEBUG_UnixToWinPath}
       logVar(WindowsFilePath, 'WindowsFilePath');
      {$ENDIF}
     {$ENDIF}
     Result := WindowsFilePath;
end;


function GetUnixDirPath(FilePath:string):string;
var
DirPath:string;
//LocalChannel:string;
begin
     DirPath := LeftStr(FilePath, (Rpos('/',FilePath) ));
     //log(0, LocalChannel, ( 'VAR ' + DirPath));

     Result := DirPath;
end;

function WorkDirTemplate(FolderPath:string):string;
begin
     Result := ('cd "' + FolderPath + '";' + #10 );
end;

{$IFDEF LogVar}
 procedure LogVar(MyVar: string; MyVarName: string);
 begin
      Log(2, 'LogVar', ( Wrap( MyVarName ) + 'is' + Wrap( MyVar )) );
 end;
{$ENDIF}

function DirExistsIfNotMakeIt(Path:string; FolderName:string ): boolean;
var
LocalChannel:string; {Ignore this for now}
begin
LocalChannel := '';

     if FolderName <> '' then
        begin
             if DirectoryExists( Path + FolderName ) = true then
                begin
                     Log(0, LocalChannel, ('Directory' + Wrap( Path + FolderName ) + 'exists.')) ;
                     Result := true;
                end
                   else
                       begin
                            Log(0, LocalChannel, ('Directory' + Wrap( Path + FolderName ) + ' does not exists.')) ;
                            if CreateDir( Path + FolderName ) then
                               begin
                                    Log(0, LocalChannel, ('File' + Wrap( Path + FolderName ) + 'has being created.')) ;
                                    Result := true;
                               end
                                  else
                                      begin
                                           Log(0, LocalChannel, ('File' + Wrap( Path + FolderName ) + 'can not be created.')) ;
                                           Result := true;
                                      end;
                       end;

        end
           else
               begin
                    Result := false;
               end;
end;

function DirExistsIfNotMakeIt(PathWithFolder:string): boolean;
var
FolderPath:string;
FolderName:string;
begin
     {E.G. PathWithFolder := '/home/test/hello/'.}

     {If PathWithfolder has '/' at the end remove it.}
     RemoveTrailingChars(PathWithFolder,['/']);

     {FolderName := 'hello'.}
     FolderName := ExtractFileName(PathWithFolder);

     {FolderPath := '/home/test/'.}
     FolderPath := ExtractFilePath(PathWithFolder);

     {$IFDEF LogVar}
     logVar(FolderName, 'FolderName');
     logVar(FolderPath, 'FolderPath');
     {$ENDIF}

     {Pass the data to real function and get the result back.}
     if DirExistsIfNotMakeIt(FolderPath, FolderName) = true then Result := true else Result := false;
end;

function MakeFile(FullPath:string;Data:string):boolean;
var
FD1:Cint;
CL:string;
begin
     CL := 'MakeFile';
     FpUnlink(FullPath);

     FD1 := fpOpen (FullPath, O_WrOnly or O_Creat);
     if FD1 > 0 then
        begin
             if length(Data)<>fpwrite (FD1,Data[1],Length(Data)) then
                begin
                     Result := False ;
                     Log(1, CL, ('When writing to' + Wrap(FullPath)) );
                end
                   else
                       begin
                            {$IFDEF MoreTrue }
                             Log(3, CL, ('File' + Wrap(FullPath) + 'has being created'));
                            {$ENDIF}
                            Result := true;
                       end;
           fpClose(FD1);
        end;
end;

function SetExecutableFlag(FullPath:string):boolean;
var
ChannelLocal:string;
begin
     {770 is owner rwx, group rwx, other nothing}
     if fpChmod (FullPath,&770) <> 0 then {0 = no error}
        begin
             Log(1, ChannelLocal, ('Can not set executable flag on' + Wrap(FullPath)) );
             Result := False;
        end
           else
               begin
                    {$IFDEF MoreTrue }
                     Log(0, ChannelLocal, ('Executable flag has being set on' + Wrap(FullPath)) );
                    {$ENDIF}
                    Result := true;
               end;
end;

function ColourCheck(Input: TEdit):boolean;
var
LocalChannel:string;
temp:string;
begin
LocalChannel := 'ColourCheck';
{This does a CaseInsensitive check to see if the file exists. Wine does not care about the executable flag. }
{I can not find FindDiskFileCaseInsensitive in the doc.}
{If it can not find the file it will return nothing and it will clear the string.}
    temp := WinToUnixPath(Input.Text);

    if FindDiskFileCaseInsensitive(temp) <> '' then
       begin
            {$IFDEF MoreTrue}Log(3, LocalChannel, 'File' + Wrap(temp) + 'exists.');{$ENDIF}
            Input.Font.Color := clgreen;
            Result := true;
       end
          else
              begin
                   Log(4, LocalChannel,'File' + Wrap(Input.Text) + 'does not exists.');
                   Input.Font.Color := clred;
                   Result := false;
              end;
end;

function CutUpFlags(Flags:string):boolean;
var
Loop:integer;
Dump:integer;
Temp:string;
begin
     UnitMain.form1.CheckListBox_Flags.Items.Clear;

     for loop := 0 to ( WordCount( Flags, [';'] )  ) do
     begin
          Dump := FindPart( ';', Flags );
          Temp  := LeftStr( Flags, Dump - 1);
          if Temp <> '' then UnitMain.form1.CheckListBox_Flags.Items.Add(Temp);
          Delete(Flags, 1, (Dump)) ;
     end;
     if Flags <> '' then UnitMain.form1.CheckListBox_Flags.Items.Add(Flags);

end;

function FindPrefixInt(PrefixName:string):integer;
var
   Loop:integer;
const
   C = 'FindPrefixInt';
begin
     {Error code is -1}
     Result := -1;

     for loop := 0 to (UnitMain.form1.ComboBox_PreFix.items.Count -1) do begin
         if Data[loop].PrefixName = PrefixName then
            begin
                 Result := loop;
                 exit;
            end;
     end;

     if Result = -1 then log(1, C, ('Prefix' + Wrap(PrefixName) + 'does not exist.'));
end;

function GetWinFileName(FullPath:string):string;
begin
     {Take path and return the file name only.}
     {d:\Somefoldername\'Setup'.exe}

     FullPath := AnsiReverseString(FullPath);
     FullPath := Copy2Symb(FullPath, '\');
     Copy2SymbDel(FullPath, '.');
     FullPath := AnsiReverseString(FullPath);

     Result := FullPath;
end;

procedure ListFileDir({0} Path:string; {1} FileList:TStrings; {2} GroupList:TStrings; {3} DirListClean:TStrings; {4} DirListPath:TStrings; {5} isFileList:Boolean);
var
  SR:TSearchRec;
begin
if FindFirst(Path + '*', faAnyFile , SR) = 0 then
begin
     repeat
           if ((SR.Attr and faDirectory = 0) = isFileList) then
           begin
                {Removes folders called "." & "..".}
                if SR.Name <> '.' then
                if SR.Name <> '..' then
                begin
                     if (DirListPath <> nil) then
                       DirListPath.Add(path + SR.Name + '/');
                     if (FileList <> nil) then
                       FileList.Add(SR.Name);
                end;
           end;
     until FindNext(SR) <> 0;
          FindClose(SR);
     end;
end;

function FolderScan(ComboBox:TComboBox; Path:string):boolean;
var
  Channel:string;
  List:Tstringlist;
  TempStr:string;
  TempInt:integer;
begin
     List := TStringlist.Create;
     ListFileDir({0} Path, {1} List, {2} nil, {3} nil, {4} nil, {5} false);
     {$IFDEF LogVar}
       {$IFDEF MOREDEBUG_FolderScan}
        LogVar(Path, (ComboBox.Name + ' Path'));
        Logvar(List.Text, (ComboBox.Name + ' List'));
       {$ENDIF}
     {$ENDIF}

     if ComboBox.Sorted = true then List.Sorted := true else List.Sorted := false;

     if List.Text <> ComboBox.Items.Text then
     begin
          {$IFDEF MOREDEBUG_FolderScan}
           log(0, Channel, (ComboBox.Name + ' does not equal the new list.'));
          {$ENDIF}
          {Store selected item.}
          if ComboBox.Text <> '' then TempStr := ComboBox.Text;

          {Having the caption set stops this code from working correctly on GTK1.}
          FullClear(ComboBox);
          ComboBox.Items := List;
          {Reselect item.}
          TempInt := ComboBox.Items.IndexOf(TempStr);
          if TempInt <> -1 then ComboBox.ItemIndex := TempInt;
     end;

     List.Destroy;
     Result := true;
end;

function SelectItem(ComboBox:TComboBox; ItemName:string):boolean;
var
  TempInt:integer;
  //Channel:string;
begin
     {Need to add some debug logging.}
     //Channel := 'SelectItem';

     TempInt := ComboBox.Items.IndexOf(ItemName);
     if TempInt <> -1 then
     begin
          if ComboBox.Items.Strings[TempInt] <> '' then
          begin
               ComboBox.ItemIndex := TempInt;
          end
             else
                 begin
                      //Log(0, Channel, '');
                 end;
     end;
end;

procedure FullClear(ComboBox:TComboBox);
begin
     {Having the caption set stops the code from working correctly on GTK1.}
     {GTK2 with style set to csDropDownList never sets the caption.}
     ComboBox.Caption := '';
     ComboBox.Clear;
end;

function SearchForBin(FileName:string):string;
begin
     Result := FileSearch(FileName, GetEnvironmentVariable('PATH'));
     {$IFDEF MOREDEBUG_SearchForBin}
      {$IFDEF LogVar}
       LogVar(Result, 'SearchForBin');
      {$ENDIF}
     {$ENDIF}
end;

function FindWine(Path:string):string;
const
ChannelLocal = 'FindWine';
begin
     {This function will look for wine in Path and sub folder bin for wine.}
     {If it finds wine it will return the full path with file name, else it will return a empty string.}
     {Path should be 'PathToWine' + DistributionName + DistributionVersion + Architecture + ComboBox_WineVersion.}
     if FileExistsAndIsExecutable((Path + '/wine'), false) = true then
     begin
          Result := (Path + '/wine');
          {$IFDEF LogVar}
           LogVar(Result, 'wine_version_full_path');
          {$ENDIF}
     end
        else
            if FileExistsAndIsExecutable((Path + '/bin/wine'), false) = true then
               begin
                    Result := (Path + '/bin/wine');
                    {$IFDEF LogVar}
                     LogVar(Result, 'wine_version_full_path');
                    {$ENDIF}
               end
                  else
                      begin
                           Log(0, ChannelLocal, 'Can not find wine');
                           Result := '';
                      end;
end;

function FCreatePrefixProcess(PrefixName:string):boolean;
var
  PathToWinePrefixCreate:string;
  Path:string;
  Trap:boolean;
const
  InstalledLayout = '/bin/wineprefixcreate';
  GitLayout = '/tools/wineprefixcreate';
begin
     if PrefixName = '' then
     begin
          log(1,'', 'No name for the prefix!');
          exit(false);
     end;

     PathToWinePrefixCreate := (PathToWine + UnitMain.form1.ComboBox_WineVersion.Text);

     {Try to find it using the installed layout.}
     if FileExistsAndIsExecutable(PathToWinePrefixCreate + InstalledLayout, false) = false then
     begin
          {Try to find it using the wine git layout.}
          if FileExistsAndIsExecutable(PathToWinePrefixCreate + GitLayout, false) = false then
          begin
               log(1,'', 'Can not find wineprefixcreate or it is not executable.');
               exit(false);
          end
             else
                 begin
                      {It's the git layout.}
                      PathToWinePrefixCreate := (PathToWinePrefixCreate + GitLayout);
                 end;
     end
        else
            begin
                 {It's the installed layout.}
                 PathToWinePrefixCreate := (PathToWinePrefixCreate + InstalledLayout);
            end;

     {Note the is no = sign in this path.}
     Path := ('"' +PathToWinePrefixCreate + '" --prefix ' + '"' + PathToPrefix + PrefixName + '"');
     {$IFDEF LogVar}
      logVar(Path, 'Path');
     {$ENDIF}
     CreatePrefixProcess.CommandLine := Path;
     CreatePrefixProcess.Execute;

     {This works but Winelauncher will hang until this process ends.}
     {This returns before it's realy done.}
     {PreFixExists will return false if you run it right after this.}
     Trap := true;
     while Trap = true do
     begin
          sleep(1);
          if CreatePrefixProcess.Running = false then
          begin
               Trap := false;
               if CreatePrefixProcess.ExitStatus = 0 then
               begin
                    Result := true;
               end
                  else
                      begin
                           log(1,'', 'Something when wrong with creating the prefix.');
                           Result := false;
                      end;
          end;
     end;
end;

end.

