{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

unit UnitCommandLine;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms;

procedure ProcessCommandLineInput;


implementation
uses UnitInitialization;

procedure ProcessCommandLineInput;
begin
     if Application.HasOption('h','help') then
        begin
             Writeln('Help your self.');
             Application.Terminate;
             exit;
        end;

     if Application.HasOption('v','version') then
        begin
             Writeln('WineLauncher: ' + WineLauncherVersion);
             Writeln(BuiltWithLazarusVersion);
             Writeln(BuiltWithFpcVersion);
             Application.Terminate;
             exit;
        end;
end;

end.

