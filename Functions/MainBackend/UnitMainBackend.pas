{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

unit UnitMainBackend;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XMLCfg, Process, BaseUnix, ComCtrls, Forms, Controls;

procedure SaveLastUsedConfig();
procedure LoadLastUsedConfig();
procedure OnWineVersionChange();

function RunWineCheck(ProgramNameOverRide:string; ProgramFlagsOverRide:string):boolean;
function ListProgramsOnDisc(ModeMe:TStrings; DriveChar:char):boolean;
function AddTabToPageControl(PageControl:TPageControl; caption:string):boolean;
function EmbedFrame(frame:TFrame; PageNumber:integer):boolean;

var
 XmlLastUsedConfig:TXMLConfig;
 LastUsedConfigLoading:boolean;

 {The next three are used by ListProgramsOnDisc.}
 LastReadLinkPath:string;
 DiscErrorText:string;
 OldPath:string;

implementation
uses
 UnitInitialization,
 UnitMain,
 UnitMisc,
 UnitSettings,
 UnitProgramsList;

procedure SaveLastUsedConfig();
begin
     if LastUsedConfigLoading = true then exit;
        Try
           {1}
           XmlLastUsedConfig.SetValue('Version', '2');
           {Was WineVersionInfo/DistributionName,DistributionVersion,Architecture.}
           XmlLastUsedConfig.SetValue('WineVersionInfo/WineVersion', UnitMain.form1.ComboBox_WineVersion.Text);
           {2}
           XmlLastUsedConfig.SetValue('WineVersionInfo/UsesTerminal', UnitMain.form1.Check_Terminal.Checked);
           XmlLastUsedConfig.SetValue('WineVersionInfo/UsesSoundWrapper', UnitMain.form1.CheckBox_UseSoundWrapper.Checked);

           {Save}
           XmlLastUsedConfig.Filename := (ConfigPath + '/LastUsedConfig.xml');
        Finally
              {Clean up}
              XmlLastUsedConfig.Flush;
        end;
end;

procedure LoadLastUsedConfig();
var
  LastUsedConfigVersion:string;
begin
     {Only run this ones wine has being scanned for.}
     LastUsedConfigLoading := true;
     XmlLastUsedConfig:= TXMLConfig.Create(nil);

     Try
        XmlLastUsedConfig.Filename := (ConfigPath + '/LastUsedConfig.xml');
        {Get the version of the config file.}
        LastUsedConfigVersion := XmlLastUsedConfig.GetValue('Version','');
        {$IFDEF LogVar}
         LogVar(LastUsedConfigVersion,'LastUsedConfigVersion');
        {$ENDIF}

        {Make sure the version value exists.}
        if LastUsedConfigVersion = '-1' then
           begin
                Log(3, Channel, 'Invalided file version, never mind we ignore it and wirte over it.');
                Exit();
           end;
        {1}
        {Setting ItemIndex does not trigger OnEditingDone.}
        {So we run Enter to update the list then we run EditingDone to check it}

        SelectItem(UnitMain.form1.ComboBox_WineVersion, XmlLastUsedConfig.GetValue('WineVersionInfo/WineVersion', ''));
        UnitMain.form1.ComboBox_WineVersionEnter(form1);
        UnitMain.form1.ComboBox_WineVersionEditingDone(form1);

        {2}
        UnitMain.form1.Check_Terminal.Checked := XmlLastUsedConfig.GetValue('WineVersionInfo/UsesTerminal', false);
        UnitMain.form1.CheckBox_UseSoundWrapper.Checked := XmlLastUsedConfig.GetValue('WineVersionInfo/UsesSoundWrapper', false);

     Finally
            XmlLastUsedConfig.Flush;
            LastUsedConfigLoading := false;
     end;
end;

function RunWineCheck(ProgramNameOverRide:string; ProgramFlagsOverRide:string):boolean;
var
 loop:integer;
 SoundWrapper:string;
 temp00:integer;
 WinePreFixRaw:string;
 WinePreFix:string;
 ChannelLocal:string;
 ProgramFlags:string;
 ProgramName:string;
 WorkDir:string;
 UnixPathWithOutFile:string;
 WinePath:string; {~/.local/wine/[Wine Version]"}
 wine_version_full:string;
 start_S1:string; { Script to write to the file. }
 RunIn:string; { terminal or shell set here }
begin
ChannelLocal := ('RunWineCheck');

WinePath := (PathToWine + UnitMain.form1.ComboBox_WineVersion.Text);

{Get wine version info}
wine_version_full := FindWine(WinePath);
if wine_version_full = '' then exit(false);


{Get Terminal info}
if UnitMain.form1.Check_Terminal.Checked = true then
begin
     if UnitMain.form1.ComboBox_TerminalName.Text <> '' then
        begin
             temp00 := UnitMain.form1.ComboBox_TerminalName.ItemIndex;
             RunIn := (UnitSettings.Form4.StringGrid_TerminalSettings.Cells[1,temp00]  + ' ' + UnitSettings.Form4.StringGrid_TerminalSettings.Cells[2,temp00]);
             Log(0, ChannelLocal, ('Using Terminal' + Wrap(RunIn)) );
        end;
end
   else
       begin
            {After the Program is closed the child process is still listed.}
            RunIn := UnitSettings.Form4.ComboBox_ShellPath.text;
            Log(0, ChannelLocal, ('Using' + Wrap(RunIn)) );
       end;

{PreFix}
WinePreFixRaw := (PreFixList[1].Strings[UnitMain.form1.ComboBox_PreFix.ItemIndex]);
WinePreFix := ('env WINEPREFIX="' + WinePreFixRaw + '"');
{$IFDEF LogVar}
 LogVar(WinePreFix, 'WinePreFix');
{$ENDIF}


{Sound Wrapper}
if UnitMain.form1.CheckBox_UseSoundWrapper.Checked = true then
begin
    if FileExists(UnitMain.form1.ComboBox_SoundWrapper.Text) = true then
       begin
            SoundWrapper := (UnitMain.form1.ComboBox_SoundWrapper.Text + ' ');
            Log(0, ChannelLocal, ('File' + Wrap(UnitMain.form1.ComboBox_SoundWrapper.Text) + 'exists'));
       end
          else
              begin
                   Log(1, ChannelLocal, ('File' + Wrap(UnitMain.form1.ComboBox_SoundWrapper.Text) + 'does not exists'));
                   exit(false);
              end;
end
  else
      begin
           Log(0, ChannelLocal, ('No sound wrapper will be used'));
      end;

{Set program flags}
if ProgramNameOverRide = '' then
begin
     if UnitMain.form1.EditBox_ProgramPath.Text <> '' then
     begin
          ProgramFlags := '';
          for loop := 0 to (UnitMain.form1.CheckListBox_Flags.Items.Count -1) do
              begin
                   if UnitMain.form1.CheckListBox_Flags.Checked[loop] = true then
                      begin
                           ProgramFlags := (ProgramFlags + ' ' + UnitMain.form1.CheckListBox_Flags.Items.ValueFromIndex[loop]);
                      end;
              end;
          {$IFDEF LogVar}
           LogVar(ProgramFlags, 'ProgramFlags' );
          {$ENDIF}
          ProgramName := (' ' + '"' + UnitMain.form1.EditBox_ProgramPath.Text + '"' + ProgramFlags + ' ');
     end
        else
            begin
                 Log(1, ChannelLocal, ('Please select a program.'));
                 exit(false);
            end;
end
   else
       begin
            ProgramName := (' ' + '"' + ProgramNameOverRide + '"' + ProgramFlagsOverRide + ' ');
       end;

{Set the WorkDir}
UnixPathWithOutFile := ExtractFilePath(WinToUnixPath(UnitMain.form1.EditBox_ProgramPath.Text));
if ProgramNameOverRide = '' then
begin
     if DoesFolderExists(ExtractFilePath(UnixPathWithOutFile), ExtractFileName(UnixPathWithOutFile)) then
     begin
          WorkDir := WorkDirTemplate(GetUnixDirPath(WinToUnixPath(UnitMain.form1.EditBox_ProgramPath.Text)) );
     end
        else
            begin
                 WorkDir := WorkDirTemplate(WinePreFixRaw + '/dosdevices/c:/windows');
        end;
end
   else
       begin
            WorkDir := WorkDirTemplate(WinePreFixRaw + '/dosdevices/c:/windows');
       end;

{Setup start_S1}
start_S1 := ('#! /bin/sh' + {linebrake}#10 + WorkDir {Line brake done in template.} + WinePreFix + SoundWrapper + ' "' + wine_version_full + '" ' +  ProgramName + ';');
{$IFDEF LogVar}
 LogVar(start_S1, 'start_S1');
{$ENDIF}

{Create Script file}
if MakeFile((ConfigPath + '/1'), start_S1) = false then
begin
     log(1, ChannelLocal, ('Can not make file' + wrap(ConfigPath + '/1')) );
     exit(false);
end;

{Set executable flag}
if SetExecutableFlag(ConfigPath + '/1') = false then
begin
     log(1, ChannelLocal, ('Can not set executable flag on file'+ wrap(ConfigPath + '/1')) );
     exit(false)
end;

{Execute script one}
Log(0, ChannelLocal, ('Script One has being executed.'));
AProcess.CommandLine := (RunIn + ' ' + ConfigPath +'/1');
{After the Program is closed the child process is still listed.}
AProcess.Execute;
Result := true;
end;

function ListProgramsOnDisc(ModeMe:TStrings; DriveChar:char):boolean;
var
 script:string;
 Path:string;
 Info:TSearchRec;
 Count:Longint;
 FindPath:string;
begin
{TODO : This function needs to check the drive not just the folders.}
Path := (PathToPrefix + UnitMain.Form1.ComboBox_PreFix.Text + '/dosdevices/' + AnsiLowerCase(DriveChar));

{Do not rescan if the path is the same as the last one.}
{Note if you change disks it will not rescan it.}
if OldPath = Path then exit(false);

if AsyncProcessScan.Active = false  then
begin
     OldPath := Path;
     ModeMe.Clear;
     LastReadLinkPath := fpReadLink(Path + ':');
end
   else exit(false);

if LastReadLinkPath = '' then
begin
     log(4, '',('Link ' + Wrap(Path + ':') + 'does not exists.'));
     LastReadLinkPath := '';
     DiscErrorText := 'Please map' + wrap(DriveChar + ':') + 'to your cd / dvd drive, You can do that in winecfg.';
     exit(false);
end;

{Reset count.}
Count := 0;
if FindFirst((Path + ':/*'), faAnyFile and faDirectory, Info) = 0 then
begin
     Repeat
           Inc(Count);
     Until FindNext(info) <> 0;
          if Count < 3 then
             begin
                  log(0,'', Info.Name);
                  DiscErrorText := 'You have no disc in your drive.';
                  LastReadLinkPath := '';
                  FindClose(Info);
                  exit(false);
             end;
end;
FindClose(Info);

if LastReadLinkPath = '/' then
begin
     DiscErrorText := 'You can not map '+ DriveChar + ': to root.';
     LastReadLinkPath := '';
     exit(false);
end;

if LastReadLinkPath = GetEnvironmentVariable('HOME') then
begin
     DiscErrorText := 'You can not map '+ DriveChar + ': to your home folder.';
     LastReadLinkPath := '';
     exit(false);
end;

FindPath := SearchForBin('find');
if FindPath = '' then
begin
     DiscErrorText := ('Can not find the program called' + Wrap('find'));
     LastReadLinkPath := '';
     exit(false);
end;

script := (FindPath + ' -H ' + Path + ': -iname *.exe');
Log(0, '', 'Script Scan has being executed.');
AsyncProcessScan.CommandLine := script;
AsyncProcessScan.Execute;
DiscErrorText := 'Scaning';
Timer.Enabled := true;

Result := true;

end;

procedure OnWineVersionChange();
begin
     if UnitMain.form1.ComboBox_WineVersion.Text <> '' then
     begin
          UnitMain.form1.Btn_Run.Enabled := true;
     end
        else
            UnitMain.form1.Btn_Run.Enabled := false;
end;

function AddTabToPageControl(PageControl:TPageControl; caption:string):boolean;
var
  Index:Integer;
begin
     Index := PageControl.PageCount ;
     SetLength(TabSheets, (Index + 1));

     {Make tab sheet.}
     TabSheets[Index] := TTabSheet.Create(TabSheets[Index]);
     TabSheets[Index].Caption := caption;
     TabSheets[Index].PageControl := PageControl;
end;

function EmbedFrame(frame:TFrame; PageNumber:integer):boolean;
begin
     frame.Parent := UnitMain.form1.PageControl1.Page[PageNumber];
     frame.Align := (alclient);
     frame.Show;
end;

end.

