#! /bin/sh
# This script Makes a backup of all your PrefixData.xml files and zips then up.
cd ~/.local/wine-prefix
mkdir ./../WinePrefixInfoBackup
find -H  ./*/PrefixData.xml | zip -@ ./../WinePrefixInfoBackup/PrefixBackup_`date +%d-%B-%Y`
