{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

unit UnitCreatePrefix;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  LResources,
  Forms,
  StdCtrls,
  Grids;


type

  { TFrameCreatePrefix }

  TFrameCreatePrefix = class(TFrame)
    Btn_CreatePrefix: TButton;
    EditBox_PrefixName: TEdit;
    Label_PrefixName: TLabel;
    procedure Btn_CreatePrefixClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  FrameCreatePrefix:TFrameCreatePrefix;

implementation
uses
 UnitMain,
 UnitMisc,
 UnitInitialization,
 UnitProgramsList,
 UnitSettings;


procedure TFrameCreatePrefix.Btn_CreatePrefixClick(Sender: TObject);
var
SelectedItemOne:string;
SelectedItemTwo:string;
Done:boolean;
begin
Done := false;

     if FCreatePrefixProcess(EditBox_PrefixName.Text) = true then
     begin
          if NoPrefix = true then
          begin
               {This is messy, Creating a prefix returns before it's realy done.}
               {So PreFixExists will return false. So we run it a in while. }
               while Done = false do
               begin
                    sleep(100);
                    if UnitSettings.form4.PreFixExists() = true then
                    begin
                         SetUpArray();
                         NoPrefix := false;
                         UnitMain.form1.Btn_Settings.Enabled := true;
                         UnitMain.form1.Button_Winecfg.Enabled := true;
                         UnitMain.form1.Button_Regedit.Enabled := true;
                         UnitMain.form1.Btn_Run.Enabled := true;
                         UnitMain.form1.TogBtn_Listdrive.Enabled := true;
                         Done := true;
                    end;
               end;
          end
             else
                 begin
                      {Add the new prefix name and path to PreFixList.}
                      PreFixList[0].add(EditBox_PrefixName.Text);
                      PreFixList[1].add(PathToPrefix + EditBox_PrefixName.Text);

                      {Increase the size of the array by one.}
                      SetLength(Data, PreFixList[0].count);
                      {Configure the new entry.}
                      SetUpGrid(PreFixList[0].count -1);

                      {Synchronous array with Comoboxs.}
                      SelectedItemOne := UnitMain.Form1.ComboBox_PreFix.Text;
                      UnitMain.Form1.ComboBox_PreFix.Items := PreFixList[0];
                      SelectItem(UnitMain.Form1.ComboBox_PreFix, SelectedItemOne);

                      SelectedItemTwo := UnitSettings.Form4.ComboBox_PreFix.Text;
                      UnitSettings.Form4.ComboBox_PreFix.Items := PreFixList[0];
                      SelectItem(UnitSettings.Form4.ComboBox_PreFix, SelectedItemTwo);
                 end;

     end
        else
            begin
                 log(1,'', 'Something when wrong with creating the prefix.');
            end;
end;

initialization
  {$I UnitCreatePrefix.lrs}

end.

