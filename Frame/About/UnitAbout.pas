{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

unit UnitAbout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, StdCtrls;

type

  { TFrameAbout }

  TFrameAbout = class(TFrame)
    Memo_About: TMemo;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  FrameAbout:TFrameAbout;

implementation

{ TFrameAbout }



initialization
  {$I UnitAbout.lrs}

end.

