{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

program WineLauncher;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  SysUtils,
  Forms,
  {Units}
  UnitInitialization,
  UnitCommandLine,
  UnitMain,
  UnitMainBackend,
  UnitSettings,
  UnitProgramsList,
  UnitCreatePrefix,
  UnitAbout;


begin
  {Stop the user from running this program as root.}
  if GetEnvironmentVariable('LOGNAME') = 'root' then
    begin
         WriteLn('You can not run WineLauncher as root.');
         WriteLn('See http://wiki.winehq.org/FAQ#head-96bebfa287b4288974de0df23351f278b0d41014');
         exit;
    end;

ProcessCommandLineInput;
 if Application.Terminated then exit;
InitializationCode;
 if Application.Terminated then exit;

  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm4, Form4);
  Application.Run;
end.

