{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

unit UnitSettings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls, BaseUnix,
  Grids, strutils;

type

  { TForm4 }


  TForm4 = class(TForm)
    ComboBox_PreFix: TComboBox;
    ComboBox_ShellPath: TComboBox;
    Label1: TLabel;
    Label8: TLabel;
    StringGrid_TerminalSettings: TStringGrid;
    StringGrid_ProgramsList: TStringGrid;

    procedure ComboBox_PreFixChange(Sender: TObject);
    procedure ComboBox_ShellPathEditingDone(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure StringGrid_ProgramsListDrawCell(Sender: TObject; aCol,
      aRow: Integer; aRect: TRect; aState: TGridDrawState);
    procedure StringGrid_ProgramsListEditingDone(Sender: TObject);
    procedure StringGrid_TerminalSettingsEditingDone(Sender: TObject);

    function PreFixExists():boolean;
  private

    procedure ProgramsUpdate();

  public
    { public declarations }
  end;

var
  Form4: TForm4;
  TerminalPathClean:Tstrings;
  TerminalPathFlags:Tstrings;
  TerrminalSettingsIsFine:boolean;
  ShellPathIsFine:boolean;
  ProgramsList:Tstrings;

implementation
uses
  UnitInitialization,
  UnitMain,
  UnitProgramsList,
  UnitMisc;

procedure TForm4.FormCreate(Sender: TObject);
var
LocalChannel:string;
begin
     LocalChannel := '';

     TerminalPathClean := TStringlist.Create;
     ProgramsList := TStringlist.Create;

     StringGrid_ProgramsList.SaveOptions := [soDesign,{soAttributes,}soContent{,soPosition}] ;


     {Cheep Hack to check the StringGrid and sych the combobox. }
     StringGrid_TerminalSettingsEditingDone(self);

     {Crash fix}
     ComboBox_ShellPath.Text := ComboBox_ShellPath.Items[0] ;

     {Load StringGrid_TerminalSettings}

     if FileExists ( ConfigPath + '/StringGrid_TerminalSettings') = true then
        begin
             {NOTE StringGrid.LoadFromFile has no error checking?  http://www.freepascal.org/docs-html/lcl/grids/tcustomgrid.loadfromfile.html}
             StringGrid_TerminalSettings.LoadFromFile( ConfigPath + '/StringGrid_TerminalSettings' );
             Log(3, LocalChannel, ( ConfigPath + '/StringGrid_TerminalSettings' + ' ) has being loaded.'));
        end
           else
               begin
                    Log(4, LocalChannel, ( ConfigPath + '/StringGrid_TerminalSettings' + ' ) does not exists.'));
               end;

{Lay out for PreFixList: array of Tstrings}
{The first Tstrings is for prefix names}
{The second Tstrings is for the paths of the prefix's}
     SetLength(PreFixList, 2);
     PreFixList[0] := TStringlist.Create;
     PreFixList[1] := TStringlist.Create;

     if PreFixExists() = true then
     begin
          SetUpArray();
     end
        else
            begin
                 UnitMain.form1.Btn_Settings.Enabled := false;
                 UnitMain.form1.Button_Winecfg.Enabled := false;
                 UnitMain.form1.Button_Regedit.Enabled := false;
                 UnitMain.form1.Btn_Run.Enabled := false;
                 UnitMain.form1.TogBtn_Listdrive.Enabled := false;
                 UnitMain.form1.EditBox_ProgramPath.Text := '';
                 UnitMain.form1.EditBox_ProgramPath.Enabled := false;
                 NoPrefix := true;
                 MessageDlg('Please create a wine prefix', 'You need to create a wine prefix see the create prefix tab.', mtWarning, [mbOK], '');
            end;

end;

procedure TForm4.FormDestroy(Sender: TObject);
begin
     PreFixList[1].Destroy;
     PreFixList[0].Destroy;
     TerminalPathClean.Destroy;
     ProgramsList.Destroy;
end;

procedure TForm4.StringGrid_ProgramsListDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
begin
     StringGrid_ProgramsList.AutoSizeColumns;
end;

procedure TForm4.StringGrid_ProgramsListEditingDone(Sender: TObject);
var
loop:integer;
LocalChannel:string;
begin
LocalChannel := 'StringGrid_ProgramsListEditingDone';

{TODO Went Editing this runs two times, abit pointless and not CPU friendy}
{TODO may add some checks to the flags. Can that be done ?}

{log EditingDone}
Log(5, LocalChannel, ('-----------------'));
Log(3, LocalChannel, ( StringGrid_ProgramsList.Name + ' editing has finished.' ));

     for loop := 1 to StringGrid_ProgramsList.Cols[DataNameCol].Count    -1 do
        begin
             if (StringGrid_ProgramsList.Cells[DataNameCol,loop]) = '' then
                begin
                     StringGrid_ProgramsList.MoveColRow(false,loop, (StringGrid_ProgramsList.Cols[DataNameCol].Count -1)) ;
                     StringGrid_ProgramsList.Rows[StringGrid_ProgramsList.Cols[DataNameCol].Count -1].Clear ;
                end;
        end;

     {Fill it}
     ProgramsUpdate();
end;

procedure TForm4.ProgramsUpdate();
var
B:integer;
PrefixInt:integer;
begin
     {Fill array}
     PrefixInt := FindPrefixInt(ComboBox_PreFix.Items.Strings[ComboBox_PreFix.ItemIndex]);
     for B := 0 to Data[PrefixInt].Grid.RowCount -1 do
         begin
              Data[PrefixInt].Grid.Rows[B].Text := StringGrid_ProgramsList.Rows[B].text;
         end;
end;

function TForm4.PreFixExists():boolean;
var
PreFixFolders:Tstrings;
loop:integer;
ChannelLocal:string;
begin
     ChannelLocal := ('PreFixExists');
     Log(5, '', ('-----------------'));
     Log(0, ChannelLocal, ('PreFixExists has being Called.'));

     PreFixFolders := TStringlist.Create;
{ TODO : ListFileDir can only handly one path at ones }
ListFileDir({0} PathToPrefix, {1} PreFixFolders, {2} nil, {3} nil, {4} nil, {5} false);

for loop := 0 to (PreFixFolders.Count -1)  do
begin
     if AnsiEndsText('~', PreFixFolders.Strings[loop]) = false then  {Igonre backup files.}
     begin
         if DoesFolderExists(PathToPrefix + PreFixFolders.Strings[loop] + '/dosdevices') = true then
         begin
              if DoesFolderExists(PathToPrefix + PreFixFolders.Strings[loop] + '/drive_c') = true then
              begin
                   if FileExistsAndIsExecutable(PathToPrefix + PreFixFolders.Strings[loop] + '/system.reg',true) = true then
                   begin
                        if FileExistsAndIsExecutable(PathToPrefix + PreFixFolders.Strings[loop] + '/user.reg', true) = true then
                        begin
                             PreFixList[0].add(PreFixFolders.Strings[loop]);
                             PreFixList[1].add(PathToPrefix + PreFixFolders.Strings[loop]);
                        end;
                   end;
              end;
         end;
     end;
end;

   if PreFixList[0].Count <> 0 then
   begin
        {Sych Tstrings with Comobox.}
        UnitMain.Form1.ComboBox_PreFix.Items := PreFixList[0];
        ComboBox_PreFix.Items := PreFixList[0];

        {Cheep hack to stop crashs.}
        UnitMain.Form1.ComboBox_PreFix.Text := UnitMain.Form1.ComboBox_PreFix.Items[0];
        ComboBox_PreFix.Text := ComboBox_PreFix.Items[0];
        Result := true;
   end
      else
          begin
               Result := false;
          end;


{Clean up.}
FreeAndNil(PreFixFolders);
end;


procedure TForm4.FormClose(Sender: TObject; var CloseAction: TCloseAction);
var
LocalChannel:string;
TempInt:Integer;
loop:Integer;
begin
{ TODO : Set LocalChannel. }
LocalChannel := '';

Log(5, LocalChannel, ('-----------------'));
Log(3, LocalChannel, ( Form4.Name + ' close has happen.' ));

{Save Prefix data.}
for loop := 0 to (PreFixList[0].Count -1) do
begin
     ProgramsListSavePrefix(PreFixList[0][loop]);
end;


{Temp save of StringGrid_TerminalSettings}
if TerrminalSettingsIsFine = true then
   begin
        Log(3, LocalChannel, ( 'Saving' + Wrap(StringGrid_TerminalSettings.Name) + 'to' + Wrap(ConfigPath + '/StringGrid_TerminalSettings') ));
        StringGrid_TerminalSettings.SaveOptions := [soDesign,{soAttributes,}soContent{,soPosition}] ;
        {TODO error checking for save.}
        StringGrid_TerminalSettings.SaveToFile( ConfigPath + '/StringGrid_TerminalSettings' );
   end
      else
          begin
               Log(1, LocalChannel, ('Not saving' + Wrap(StringGrid_TerminalSettings.Name) + 'because the is something wrong with it.'));
          end;

if Mode = false then
   begin
        {Refresh form1.}
        {Remember the selected item.}
        TempInt := UnitMain.Form1.ComboBox_ProgramsList.ItemIndex;
        {Update ComboBox_PreFix, This clears ComboBox_ProgramsList.}
        UnitMain.Form1.ComboBox_PreFixChange(self);
        {Write back the remembered selected item.}
        UnitMain.Form1.ComboBox_ProgramsList.ItemIndex := TempInt;
        {This updates EditBox_ProgramPath.}
        UnitMain.Form1.ComboBox_ProgramsListChange(self);

        {Note flag settings are losted.}
   end;

end;

procedure TForm4.ComboBox_ShellPathEditingDone(Sender: TObject);
var
LocalChannel:string;
begin
{ TODO : Set LocalChannel. }
LocalChannel := '';

Log(5, LocalChannel, ('-----------------'));
Log(3, LocalChannel, ( ComboBox_ShellPath.Name + ' editing has finished.' ));

if FileExistsAndIsExecutable( {FullPath:string}ComboBox_ShellPath.text, {JustCheckExists:boolean}false ) = true then
   begin
        ShellPathIsFine := true;
{ TODO -cColour : Work out why "clwindow" does not work. }
        ComboBox_ShellPath.Color := clwhite;
   end
      else
          begin
               ShellPathIsFine := false;
               ComboBox_ShellPath.Color := clred;
          end;

end;

procedure TForm4.ComboBox_PreFixChange(Sender: TObject);
var
B:integer;
PrefixInt:integer;
begin
     StringGrid_ProgramsList.Clean ;
     StringGrid_ProgramsList.FixedRows := 1 ;
     PrefixInt := FindPrefixInt(ComboBox_PreFix.Items.Strings[ComboBox_PreFix.ItemIndex]);

     for B := 0 to 100 do
        begin
             StringGrid_ProgramsList.Rows[B].text := Data[PrefixInt].Grid.Rows[B].Text;
        end;
end;


procedure TForm4.StringGrid_TerminalSettingsEditingDone(Sender: TObject);
var
LocalChannel:string;
loop:integer;
loop2:integer;
IsFine:boolean;
begin
{ TODO : Went Editing this runs two times, abit pointless and not CPU friendy}
{ TODO : May add some checks to the flags. Can that be done ?}
{ TODO : Set LocalChannel. }
LocalChannel := '';
IsFine := true;


Log(5, LocalChannel, ('-----------------'));
Log(3, LocalChannel, ( StringGrid_TerminalSettings.Name + ' editing has finished.'));

{USEFUL INFO: stringGrid.Cells[col,row] }

{Check the no row is blank other then the last one}
if IsFine = true then
   begin
        for loop := 0 to ( StringGrid_TerminalSettings.RowCount  -2 )do {Ignore last row}
            begin
                 {TODO : check the whole row not just the frist cell }
                 if StringGrid_TerminalSettings.Cells[0,loop] = '' then {delete it!}
                    begin
                         StringGrid_TerminalSettings.DeleteColRow(false,loop);
                         Log(4, LocalChannel, ('Not Fine deleting row.'));
                         exit; {exit or out of range error}
                    end
                       else
                           begin
                                //
                           end;
            end; {end loop}
   end;


{Check the Names are unique or you can get the wrong path}
if IsFine = true then
   begin
        //UnitMain.form1.log('Running Check the Names are unique');
        for loop := 1 to ( StringGrid_TerminalSettings.RowCount  -1 )do
            begin
                 for loop2 := 0 to ( StringGrid_TerminalSettings.RowCount  -1 )do
                     begin
                          if StringGrid_TerminalSettings.cells[0,loop] = StringGrid_TerminalSettings.cells[0,loop2] then
                             begin
                                  if loop2 <> loop then
                                     begin
                                          { Error }
                                          Log(4, LocalChannel, ( 'Col(0)Row(' + inttostr(loop) + ') has the same name as Col(0)Row(' + inttostr(loop2) + ').'));
                                          IsFine := false;
                                     end
                                        else
                                            begin
                                                 { All fine }
                                            end;
                            end;
                      end; {loop2 end}
             end;{loop end}
   end;


{Check that Path does exists and is executable.}
if IsFine = true then
   begin
        for loop := 0 to ( StringGrid_TerminalSettings.RowCount  -1 )do
            begin
                 if loop = ( StringGrid_TerminalSettings.RowCount  -1 ) then
                    begin
                         if StringGrid_TerminalSettings.Cells[0,loop] <> '' then
                            begin
                                 if FileExists( StringGrid_TerminalSettings.Cells[1,loop] ) then
                                    begin
                                         Log(3, LocalChannel, ('Col(1)Row(' + inttostr(loop) + ')  file ( ' +  StringGrid_TerminalSettings.Cells[1,loop] + ' ) does exists.'));
                                         if FpAccess( StringGrid_TerminalSettings.Cells[1,loop] , X_OK {Exe flag check}) = 0  then
                                            begin
                                                 Log(1, LocalChannel, ('Col(1)Row(' + inttostr(loop) + ') file ( ' + StringGrid_TerminalSettings.Cells[1,loop] + ' ) is executable.'));
                                            end
                                               else
                                                   begin
                                                        { else FpAccess }
                                                        IsFine := false;
                                                        Log(1, LocalChannel, ('Col(1)Row(' + inttostr(loop) + ') file  (' + StringGrid_TerminalSettings.Cells[1,loop] + ' ) is not executable.'));
                                                   end;
                                    end
                                       else
                                           begin
                                                { else FileExists }
                                                IsFine := false;
                                                Log(1, LocalChannel, ('Col(1)Row(' + inttostr(loop) + ') file ( ' +  StringGrid_TerminalSettings.Cells[1,loop] + ' ) does not exists.'));
                                           end;
                      end; { StringGrid_TerminalSettings.Cells[0,loop] <> '' END }
             end
                else { ELSE loop = ( StringGrid_TerminalSettings.RowCount  -1 )  }
                     begin
                          //
                     end;

           end; {loop end}
end;

{if last row is used then add a new one}
if IsFine = true then
   begin
        if StringGrid_TerminalSettings.cells[0,(StringGrid_TerminalSettings.RowCount  -1)]  <> '' then
           begin
                StringGrid_TerminalSettings.RowCount := ( StringGrid_TerminalSettings.RowCount + 1 );
           end;
   end;

{Report!}
if Isfine = true then
   begin
        TerrminalSettingsIsFine := true;
        Log(0, LocalChannel, ('Terminal settings are fine.'));
   end
      else
          begin
               TerrminalSettingsIsFine := false;
               Log(1, LocalChannel, ('The is an error in the terminal settings.'));
          end;


{sych}
if Isfine = true then
   begin
        TerminalPathClean.Clear;
        for loop := 0 to ( StringGrid_TerminalSettings.RowCount  -2 )do  {Last row is ignore WILL be blank}
            begin
                 TerminalPathClean.Add(StringGrid_TerminalSettings.Cells[0,loop] );
            end;

       UnitMain.Form1.ComboBox_TerminalName.Items := TerminalPathClean;
        {INFO this will change your seleted item back to the first item, not the worlds best code but it stops crashs}
        UnitMain.Form1.ComboBox_TerminalName.Text := UnitMain.Form1.ComboBox_TerminalName.Items[0];
   end;

end;




initialization
  {$I UnitSettings.lrs}

end.

