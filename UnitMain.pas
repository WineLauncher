{   This file is part of WineLauncher.

    WineLauncher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WineLauncher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WineLauncher.  If not, see <http://www.gnu.org/licenses/>.
}

 {Made with Lazarus 0.9.28.2 fixes branch.}
 {Free Pascal Compiler version 2.2.5.}

unit UnitMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  DbCtrls, BaseUnix, Process, CheckLst, ExtCtrls, ComCtrls, UnitCreatePrefix, UnitAbout, Errors;

type

  { TForm1 }

  TForm1 = class(TForm)
    Btn_Run: TButton;
    Btn_Settings: TButton;
    Btn_StopScan: TButton;
    Button_Regedit: TButton;
    Button_Winecfg: TButton;
    CheckBox_UseSoundWrapper: TCheckBox;
    CheckListBox_Flags: TCheckListBox;
    Check_Terminal: TCheckBox;
    ComboBox_WineVersion: TComboBox;
    ComboBox_ProgramsList: TComboBox;
    ComboBox_PreFix: TComboBox;
    ComboBox_SoundWrapper: TComboBox;
    ComboBox_TerminalName: TComboBox;
    EditBox_ProgramPath: TEdit;
    Label_WineVersion: TLabel;
    Label_Flags: TLabel;
    Label_ProgramPath: TLabel;
    Label_ProgramsList: TLabel;
    Label_WinePreFix: TLabel;
    Memo_LogOutPut: TMemo;
    PageControl1: TPageControl;
    Tab_Log: TTabSheet;
    TogBtn_Listdrive: TToggleBox;

    procedure Btn_RunClick(Sender: TObject);
    procedure Btn_SettingsClick(Sender: TObject);
    procedure Btn_StopScanClick(Sender: TObject);
    procedure Button_RegeditClick(Sender: TObject);
    procedure Button_WinecfgClick(Sender: TObject);
    procedure ComboBox_PreFixChange(Sender: TObject);
    procedure ComboBox_ProgramsListChange(Sender: TObject);
    procedure ComboBox_WineVersionEditingDone(Sender: TObject);
    procedure ComboBox_WineVersionEnter(Sender: TObject);
    procedure EditBox_ProgramPathChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TogBtn_ListdriveChange(Sender: TObject);
    procedure TimerOnTimer(Sender: TObject);

  private { private declarations }

  public { public declarations }

  end;

var
  Form1: TForm1;
  NoError:Boolean; { Tbh this should not be here but im not sure need to check. }
  Sh_Path:string; { Path for the shell }
  GetCurrentDir:string;
  Channel:string; { Debug channel name }
  UnixDirPath:string; { Needed for setting the working dir at 'Run'. }
  Mode:boolean;
  Timer:TTimer; {The timer is a workaround.}
  {Frames}
   FrameCreatePrefix:TFrameCreatePrefix;
   FrameAbout:TFrameAbout;

implementation
uses
  UnitInitialization,
  UnitMainBackend,
  UnitMisc,
  UnitSettings,
  UnitProgramsList;

procedure TForm1.FormCreate(Sender: TObject);
begin
     {The timer is a workaround.}
     Timer := TTimer.Create(nil) ;
     Timer.OnTimer := @TimerOnTimer;
     Timer.Enabled := false;
     Timer.Interval := 100;

     {Scan for wine.}
     ComboBox_WineVersionEnter(Self);

     {Select first item.}
     if ComboBox_SoundWrapper.items.Count <> 0 then ComboBox_SoundWrapper.Text := ComboBox_SoundWrapper.Items[0];
     if ComboBox_TerminalName.items.Count <> 0 then ComboBox_TerminalName.Text := ComboBox_TerminalName.Items[0];
     if ComboBox_WineVersion.Items.Count <> 0 then ComboBox_WineVersion.Text := ComboBox_WineVersion.Items[0];

     {Frame Create prefix.}
     AddTabToPageControl(PageControl1, 'Create prefix');
     FrameCreatePrefix := TFrameCreatePrefix.Create(Self);
     EmbedFrame(FrameCreatePrefix, PageControl1.PageCount -1);
     {Frame about.}
     AddTabToPageControl(PageControl1, 'About');
     FrameAbout := TFrameAbout.Create(Self);
     EmbedFrame(FrameAbout, PageControl1.PageCount -1);

     FrameAbout.Memo_About.Lines.Add('');
     {When the is a new release update the version number in the file called 'VERSION.inc'.}
     FrameAbout.Memo_About.Lines.Add('WineLauncher Version: ' + WineLauncherVersion);
     FrameAbout.Memo_About.Lines.Add(BuiltWithLazarusVersion);
     FrameAbout.Memo_About.Lines.Add(BuiltWithFpcVersion);

     {Load LastUsedConfig.}
     LoadLastUsedConfig();

     {$IFDEF GuiTest}
      Btn_StopScan.Visible := true;
     {$ENDIF}
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
     Timer.Destroy;
     XmlLastUsedConfig.Destroy;
     SetLength(TabSheets, 0);
     SetLength(Data, 0);
     FreeAndNil(CdData);
     FreeAndNil(CdDataName);

     {stuff configed in UnitInitialization.}
     AProcess.Destroy;
     CreatePrefixProcess.Destroy;
     AsyncProcessScan.Destroy;
end;

procedure TForm1.TimerOnTimer(Sender: TObject);
var
TempList:Tstrings;
begin
     {We should not use a timer to check this. The is a bug in lazarus, 'OnTerminate' of AsyncProcess never runs.}
     if AsyncProcessScan.Active = true  then
        begin
             Timer.Enabled := true;
             Btn_StopScan.Visible := true;
        end
           else
               begin
                    Btn_StopScan.Visible := false;
                    TempList := TStringlist.Create;
                    Timer.Enabled := false;
                    TempList.LoadFromStream(AsyncProcessScan.OutPut);

                    if TempList.Count = 0 then
                    begin
                         DiscErrorText := 'Drive' + wrap('D' + ':') + 'has no executable files.';
                         LastReadLinkPath := '';
                    end
                       else
                           begin
                                DiscErrorText := '';
                           end;

                    while TempList.Count <> 0 do
                    begin
                         CdData.Add(UnixToWinPath(TempList.Strings[0],'d'));
                         TempList.Delete(0);
                    end;

                    ComboBox_PreFixChange(self);
                    TempList.Destroy;

                    CheckListBox_Flags.Clear;
               end;
end;

procedure TForm1.TogBtn_ListdriveChange(Sender: TObject);
begin
     if Mode = true then
     begin
          Mode := false;
          AsyncProcessScan.Active := false;
          Btn_StopScan.Visible := false;
          CheckListBox_Flags.Enabled := true;
          EditBox_ProgramPath.Clear;

          FreeAndNil(CdData);
          FreeAndNil(CdDataName);

          ComboBox_PreFixChange(self);
          ComboBox_ProgramsListChange(self);
     end
        else
            begin
                 Mode := true;
                 CdData := TStringlist.Create;
                 CdDataName := TStringlist.Create;

                 log(0,'', LastReadLinkPath);
                 ComboBox_PreFixChange(self);
                 CheckListBox_Flags.Clear;
                 EditBox_ProgramPath.Clear;
            end;
end;




procedure TForm1.Btn_RunClick(Sender: TObject);
begin
     RunWineCheck( '', '' );
end;

procedure TForm1.Btn_SettingsClick(Sender: TObject);
begin
     UnitSettings.Form4.Show ;
end;

procedure TForm1.Btn_StopScanClick(Sender: TObject);
begin
     AsyncProcessScan.Active := false;
end;

procedure TForm1.Button_RegeditClick(Sender: TObject);
begin
     RunWineCheck( 'Regedit', '' );
end;

procedure TForm1.Button_WinecfgClick(Sender: TObject);
begin
     RunWineCheck( 'winecfg', '' );
end;


procedure TForm1.ComboBox_PreFixChange(Sender: TObject);
var
   myloop:integer;
begin
     ComboBox_ProgramsList.Items.Clear;

EditBox_ProgramPath.Enabled := true;
ComboBox_ProgramsList.Enabled := true;

if Mode = false then
   begin
        for myloop := 1 to (Data[ComboBox_PreFix.ItemIndex].Grid.RowCount -1) do
              begin
                   if Data[ComboBox_PreFix.ItemIndex].Grid.Cells[DataNameCol, myloop] <> '' then
                      begin
                           ComboBox_ProgramsList.Items.Add(Data[ComboBox_PreFix.ItemIndex].Grid.Cells[DataNameCol, myloop]);
                      end
                         else
                             begin
                                  break;
                             end;
              end;

if ComboBox_ProgramsList.Items.Count <> 0 then
   begin
        ComboBox_ProgramsList.ItemIndex := 0;
        EditBox_ProgramPath.Enabled := true;
        ComboBox_ProgramsList.Enabled := true;
        ComboBox_ProgramsListChange(Self);
   end
      else
          begin
               ComboBox_ProgramsList.Text := '';
               EditBox_ProgramPath.Text := '';
               EditBox_ProgramPath.Enabled := false;
               ComboBox_ProgramsList.Enabled := false;
          end;

end
   else
       begin
            ListProgramsOnDisc(CdData, 'D');
            if CdData.Text  <> '' then
            begin
            CdDataName.Clear;
            CdDataName.AddStrings(CdData);

            {Write some code to clean up the names, keep then usefull and unique.}

            ComboBox_ProgramsList.Items := CdDataName;
            if ComboBox_ProgramsList.Items.Count <> 0 then
               begin
                    ComboBox_ProgramsList.Text := ComboBox_ProgramsList.Items[0];
                    EditBox_ProgramPath.Enabled := true;
                    ComboBox_ProgramsList.Enabled := true;
                    ComboBox_ProgramsListChange(self);
               end;

            end
               else
                   begin
                        ComboBox_ProgramsList.Items.Add(DiscErrorText);
                        ComboBox_ProgramsList.ItemIndex := 0;
                        EditBox_ProgramPath.Text := '';
                        EditBox_ProgramPath.Enabled := false;
                        ComboBox_ProgramsList.Enabled := false;
                        CheckListBox_Flags.Clear;
                   end;

       end;
end;

procedure TForm1.ComboBox_ProgramsListChange(Sender: TObject);
var
ProgramsListSelectedItem:integer;
PrefixInt:integer;
begin
     if Mode = false then
     begin
          PrefixInt := FindPrefixInt(ComboBox_PreFix.Items.Strings[ComboBox_PreFix.ItemIndex]);
          EditBox_ProgramPath.Enabled := true;
          {Plus one to line it up with the grid}
          ProgramsListSelectedItem := (Data[PrefixInt].Grid.cols[DataNameCol].IndexOf(Data[PrefixInt].Grid.cols[DataNameCol].Strings[ComboBox_ProgramsList.ItemIndex]) +1);
          {Cells[Col, Row]}
          EditBox_ProgramPath.Text := Data[PrefixInt].Grid.Cells[DataPathCol, ProgramsListSelectedItem] ;
          CutUpFlags(Data[PrefixInt].Grid.Cells[DataFlagsCol, ProgramsListSelectedItem]);
     end
        else
            begin
                 if CdData.Text <> '' then
                 begin
                      EditBox_ProgramPath.Text := CdData.Strings[CdDataName.IndexOf(ComboBox_ProgramsList.Text)]  ;
                 end;
     end;
end;

procedure TForm1.ComboBox_WineVersionEditingDone(Sender: TObject);
begin
     OnWineVersionChange();
end;

procedure TForm1.ComboBox_WineVersionEnter(Sender: TObject);
var
 a:string;
 SystemSidePath:string;
begin
     {Note FpSymLink does not have a string version with fpc 2.2.5.}
     {This is not best way adding the system side wine but it works.}
     SystemSidePath := PathToWine + '/System Side Wine';
     {link wine.}
     a := SearchForBin('wine');
     if SearchForBin(a) <> '' then
     begin
          if DirExistsIfNotMakeIt(SystemSidePath) = true then
          begin
               if DirExistsIfNotMakeIt(SystemSidePath + '/bin') = true then
               begin
                    SystemSidePath := (SystemSidePath + '/bin');
                    FpUnlink(SystemSidePath + '/wine');
                    if FpSymLink(PChar(a), PChar(SystemSidePath + '/wine') ) <> 0 then
                    begin
                         log(1,'','Can not create symlink: ' + StrError(fpgeterrno));
                    end;
                    {link wineprefixcreate.}
                    a := SearchForBin('wineprefixcreate');
                    if SearchForBin(a) <> '' then
                    begin
                         FpUnlink(SystemSidePath + '/wineprefixcreate');
                         if FpSymLink(PChar(a), PChar(SystemSidePath + '/wineprefixcreate') ) <> 0 then
                         begin
                              log(1,'','Can not create symlink: ' + StrError(fpgeterrno));
                         end;
                   end;
               end;
          end;
     end;

     FolderScan(ComboBox_WineVersion, PathToWine);
     OnWineVersionChange();
end;

procedure TForm1.EditBox_ProgramPathChange(Sender: TObject);
begin
     ColourCheck(EditBox_ProgramPath);
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
     SaveLastUsedConfig();
end;

initialization
  {$I UnitMain.lrs}

end.

